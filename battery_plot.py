#!/usr/bin/env python

from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter
from matplotlib.dates import HourLocator, MinuteLocator, DayLocator
import os
import numpy as np


class Record(object):
    def __init__(self, rec_str):
        rec = rec_str.split()
        year = int(rec[0][:4])
        month = int(rec[0][5:7])
        day = int(rec[0][8:10])
        hour = int(rec[1][:2])
        minute = int(rec[1][3:5])
        second = int(rec[1][6:8])

        self.time = datetime(year, month, day, hour, minute, second)
        self.voltage = float(rec[3])
        self.voltage_unit = rec[4]
        self.current = float(rec[7])
        self.current_unit = rec[8]

def read_log_data(filename):
    datafile = open(filename, 'r')
    datalines = datafile.readlines()

    records = [Record(l) for l in datalines if 'Node booting up' not in l]

    currents = [x.current for x in records]
    timestamps = [x.time for x in records]
    voltages = [x.voltage for x in records]

    return (timestamps, currents, voltages)

def plot_log_data(filename, dest=None, nodeid=None):


    #cd into destination directory
    if dest:
        os.chdir(dest)
    else:
        os.chdir(os.path.dirname(filename))

    timestamps, currents, voltages = read_log_data(filename)


    last_stamp = timestamps[-1].strftime("%m/%d %H:%M:%S")

    last_stamp = timestamps[-1].strftime("%m/%d %H:%M:%S")

    current_fig, current_ax = plt.subplots()
    current_ax.plot_date(timestamps, currents,'-')
    current_ax.grid()
    current_ax.xaxis.set_major_locator(DayLocator())
    current_ax.xaxis.set_major_formatter(DateFormatter('%m/%d'))
    current_ax.xaxis.set_minor_locator(HourLocator(interval=6))
    if nodeid:
        current_ax.set_title('Node {}: Logged Currents @ {}'.format(nodeid, last_stamp))
    else:
        current_ax.set_title('Logged Currents @ {}'.format(last_stamp))
    current_ax.set_ylabel('Current (mA)')
    current_ax.set_xlabel('date')
    labels = current_ax.get_xticklabels()
    plt.setp(labels, rotation=30,fontsize=10)
    plt.draw()
    plt.savefig('current.png')

    volt_fig, volt_ax = plt.subplots()
    volt_ax.plot_date(timestamps, voltages, '-')
    volt_ax.grid()
    volt_ax.xaxis.set_major_locator(DayLocator())
    volt_ax.xaxis.set_major_formatter(DateFormatter('%m/%d'))
    volt_ax.xaxis.set_minor_locator(HourLocator(interval=6))
    if nodeid:
        volt_ax.set_title('Node {}: Logged Voltages @ {}'.format(nodeid, last_stamp))
    else:
        volt_ax.set_title('Logged Voltages @ {}'.format(last_stamp))
    volt_ax.set_ylabel('Voltage (V)')
    volt_ax.set_xlabel('date')
    labels = volt_ax.get_xticklabels()
    plt.setp(labels, rotation=30,fontsize=10)
    plt.draw()
    plt.savefig('voltage.png')

def plot_overlay(filename, dest=None, nodeid=None):
    #cd into destination directory
    if dest:
        os.chdir(dest)
    else:
        os.chdir(os.path.dirname(filename))

    timestamps, currents, voltages = read_log_data(filename)

    last_stamp = timestamps[-1].strftime("%m/%d %H:%M:%S")

    fig, ax = plt.subplots()
    ax.plot_date(timestamps, currents, 'b-')
    ax.grid()
    ax.xaxis.set_major_locator(DayLocator())
    ax.xaxis.set_major_formatter(DateFormatter('%m/%d'))
    ax.xaxis.set_minor_locator(HourLocator(interval=6))
    ax.set_xlabel('Date')
    ax.set_ylabel('Current (mA)', color='k')

    for tl in ax.get_yticklabels():
        tl.set_color('b')


    if nodeid:
        ax.set_title('Node {}: Current & Voltage @ {}'.format(nodeid, last_stamp))
    else:
        ax.set_title('Current & Voltage @ {}'.format(last_stamp))

    ax2 = ax.twinx()
    ax2.plot_date(timestamps, voltages, 'r-')
    ax2.set_ylabel('Voltage (V)', color='r')
    for tl in ax2.get_yticklabels():
        tl.set_color('r')

    labels = ax.get_xticklabels()
    plt.setp(labels, rotation=30, fontsize=10)
    plt.draw()
    plt.savefig('overlay.png')

def plot_power(filename, dest=None, nodeid=None):
    #cd into destination directory
    if dest:
        os.chdir(dest)
    else:
        os.chdir(os.path.dirname(filename))

    timestamps, currents, voltages = read_log_data(filename)

    last_stamp = timestamps[-1].strftime("%m/%d %H:%M:%S")

    currents = np.array(currents)
    voltages = np.array(voltages)

    currents *= 10**-3

    power = voltages * currents

    fig, ax = plt.subplots()

    ax.plot_date(timestamps, power, '-')
    ax.grid()
    ax.xaxis.set_major_locator(DayLocator())
    ax.xaxis.set_major_formatter(DateFormatter('%m/%d'))
    ax.xaxis.set_minor_locator(HourLocator(interval=6))
    ax.set_xlabel('Date')
    ax.set_ylabel('Power (W)')

    if nodeid:
        ax.set_title('Node {}: Power @ {}'.format(nodeid, last_stamp))
    else:
        ax.set_title('Power @ {}'.format(last_stamp))

    labels = ax.get_xticklabels()
    plt.setp(labels, rotation=30, fontsize=10)
    plt.draw()
    plt.savefig('power.png')


    last_stamp = timestamps[-1].strftime("%m/%d %H:%M:%S")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Plot battery data and save image files."
    )
    parser.add_argument("filename",
        help="path to log file.")
    parser.add_argument('-o',dest="dest",
        default=None,
        help="path to destination directory")
    parser.add_argument('-n', dest='nodeid',
        help='node ID',
        default=None)

    args = parser.parse_args()
    plot_log_data(args.filename, dest=args.dest, nodeid=args.nodeid)
    plot_overlay(args.filename, dest=args.dest, nodeid=args.nodeid)
