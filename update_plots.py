#!/usr/bin/env python

import os
from battery_plot import plot_log_data, plot_overlay, plot_power
from datetime import datetime
import pickle

def find_latest_boot(filename, dest=None):
    if dest:
        os.chdir(dest)
    else:
        os.chdir(os.path.dirname(filename))

    f = open(filename, 'r')
    boot = [b.strip('\x00') for b in f.readlines() if 'booting up' in b][-1]
    print boot
    print len(boot)
    boot = boot.strip()
    print len(boot)
    f.close()
    
    print 'boot[:4] ',boot[:4]

    year = int(boot[:4])
    month = int(boot[5:7])
    day = int(boot[8:10])
    hour = int(boot[11:13])
    minute = int(boot[14:16])
    second = int(boot[17:19])

    bootdate = datetime(year, month, day, hour, minute, second)

    target_dir = os.path.dirname(filename)
    ofile = open(os.path.join(target_dir, 'boot.txt'), 'w')
    pickle.dump(bootdate, ofile)


if __name__ == "__main__":
    import MySQLdb


    db = MySQLdb.connect(user='pistat', passwd='pulsar', db='nodesdb')
    c = db.cursor()

    query = '''SELECT log_path FROM nodes'''
    c.execute(query)
    rv = c.fetchall()

    log_dirs = [row[0] for row in rv]
    print log_dirs
    node_ids = [x.split('/')[-2] for x in log_dirs]

    for i in range(len(log_dirs)):
        print i
        filename = os.path.join(log_dirs[i],'battery.log')
        plot_log_data(filename, nodeid=node_ids[i])
        plot_overlay(filename, nodeid=node_ids[i])
        plot_power(filename, nodeid=node_ids[i])
        print filename
        find_latest_boot(filename)
